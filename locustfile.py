import os
import random
from types import FunctionType

from locust import TaskSet, between, events, task
from locust.user.users import HttpUser, UserMeta
import requests

ARGUMENTS_MAYAN_EDMS_API_TOKEN_DEST = 'Mayan EDMS API token'
RESOURCE_LIMIT = 100
RESOURCES = {
    'announcements': {
        'id_field': 'id',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'assets': {
        'id_field': 'id',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'cabinets': {
        'id_field': 'id',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    # ~ 'checkouts': {
        # ~ 'id_field': 'id',
        # ~ 'views': {
            # ~ 'detail': {
                # ~ 'factory': 'TaskFactoryGetResourceDetail'
            # ~ },
            # ~ 'list': {
                # ~ 'factory': 'TaskFactoryGetResourceList'
            # ~ },
        # ~ }
    # ~ },
    'content_types': {
        'id_field': 'id',
        'views': {
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'document_types': {
        'id_field': 'id',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    #'documents': {
    #    'id_field': 'id',
    #    'views': {
    #        'detail': {
    #            'factory': 'TaskFactoryGetResourceDetail'
    #        },
    #        'list': {
    #            'factory': 'TaskFactoryGetResourceList'
    #        },
    #    }
    #},
    'event_types': {
        'id_field': 'id',
        'views': {
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'events': {
        'id_field': 'id',
        'views': {
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'groups': {
        'id_field': 'id',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'index_instances': {
        'id_field': 'id',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'keys': {
        'id_field': 'id',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'metadata_types': {
        'id_field': 'id',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'notifications': {
        'id_field': 'id',
        'views': {
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'permissions': {
        'id_field': 'id',
        'views': {
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'roles': {
        'id_field': 'id',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'search_models': {
        'id_field': 'id',
        'views': {
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'smart_links': {
        'id_field': 'id',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'tags': {
        'id_field': 'id',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'templates': {
        'id_field': 'name',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'trashed_documents': {
        'id_field': 'id',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'users': {
        'id_field': 'id',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'web_links': {
        'id_field': 'id',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
    'workflow_templates': {
        'id_field': 'id',
        'views': {
            'detail': {
                'factory': 'TaskFactoryGetResourceDetail'
            },
            'list': {
                'factory': 'TaskFactoryGetResourceList'
            },
        }
    },
}


class TaskFactoryBase:
    """Base class for task factories."""
    def __init__(self, taskset_factory, resource_name, resource_data, task_name):
        self.taskset_factory = taskset_factory
        self.resource_name = resource_name
        self.resource_data = resource_data
        self.task_name = task_name
        self.__name__ = task_name


class TaskFactoryGetResourceList(TaskFactoryBase):
    def __call__(self, user):
        user.client.get(
            '/api/v4/{}/'.format(self.resource_name), name=self.task_name
        )


class TaskFactoryGetResourceDetail(TaskFactoryBase):
    def __call__(self, user):
        resource_list = self.taskset_factory._api_resources[self.resource_name]['list']
        if len(resource_list):
            resource_selected = random.choice(
                self.taskset_factory._api_resources[self.resource_name]['list']
            )

            user.client.get(
                '/api/v4/{resource_name}/{resource_id}/'.format(
                    resource_name=self.resource_name,
                    resource_id=resource_selected[self.resource_data['id_field']]
                ), name=self.task_name
            )


class TaskSetFactory:
    _task_factory_registry = {}

    @classmethod
    def get(cls, name):
        return cls._task_factory_registry[name]

    @classmethod
    def register(cls, klass):
        cls._task_factory_registry[klass.__name__] = klass

    def __init__(self, resource_limit=None):
        self.resource_limit = resource_limit
        self._api_resources = {}

    def _api_paged_response_iterator(self, response):
        # Since API responses include only a limited number of results,
        # this iterator will perform next page requests automatically,
        # and give the impression of a continuous flow of API results.
        while True:
            response_data = response.json()

            for item in response_data['results']:
                yield item

            if response_data['next']:
                response = self._session.get(url=response_data['next'])
                response.raise_for_status()
            else:
                break

    def generate(self):
        self.task_sets = []

        for resource_name, resource_data in RESOURCES.items():
            _tasks = {}
            for view_name, view_data in resource_data['views'].items():
                task_name = '{}-{}'.format(resource_name, view_name)
                factory_name = view_data['factory']
                factory = self.get(name=factory_name)

                _tasks[
                    factory(
                        taskset_factory=self, resource_name=resource_name,
                        resource_data=resource_data, task_name=task_name
                    )
                ] = 1

            class MayanAPIResourceTaskSet(TaskSet):
                tasks = _tasks
                @task
                def stop(self):
                    self.interrupt()

            MayanAPIResourceTaskSet.__name__ = '{}APITaskSet'.format(resource_name.capitalize().replace('_', ''))
            self.task_sets.append(MayanAPIResourceTaskSet)

        return self.task_sets

    def populate(self, user):
        headers = {
            'Authorization': 'Token {}'.format(user._get_mayan_api_token())
        }
        self._session = requests.Session()
        self._session.headers.update(headers)

        """Load valid resource records from the API."""
        for resource_name in RESOURCES:
            if resource_name not in self._api_resources:
                response = self._session.get(
                    url='{}/api/v4/{}/'.format(user.host, resource_name)
                )
                response.raise_for_status()

                self._api_resources.setdefault(
                    resource_name, {
                        'list': []
                    }
                )

                for resource in self._api_paged_response_iterator(response=response):
                    self._api_resources[resource_name]['list'].append(resource)

                    if self.resource_limit:
                        if len(self._api_resources[resource_name]['list']) >= self.resource_limit:
                            break

        print('Mayan EDMS API resources.')
        print('=' * 20)
        for resource_name, data in self._api_resources.items():
            print('{}: {}'.format(resource_name, len(data['list'])))


TaskSetFactory.register(klass=TaskFactoryGetResourceDetail)
TaskSetFactory.register(klass=TaskFactoryGetResourceList)


class MayanUserMeta(UserMeta):
    def __new__(mcs, classname, bases, class_dict):
        class_dict['taskset_factory'] = TaskSetFactory(
            resource_limit=RESOURCE_LIMIT
        )
        class_dict['tasks'] = class_dict['taskset_factory'].generate()

        new_class = super(MayanUserMeta, mcs).__new__(
            mcs=mcs, classname=classname, bases=bases, class_dict=class_dict
        )
        return new_class



@events.init_command_line_parser.add_listener
def _(parser):
    parser.add_argument(
        '--mayan-api-token', type=str, env_var='MAYAN_API_TOKEN',
        default='', help='API token used to connect to the Mayan EDMS '
        'server. Must be for a user with view permissions for the API '
        'endpoints to be tested.',
        metavar='Mayan EDMS API token',
        dest=ARGUMENTS_MAYAN_EDMS_API_TOKEN_DEST
    )


class MayanRESTAPIUser(HttpUser, metaclass=MayanUserMeta):
    #wait_time = between(0.5, 1.5)
    wait_time = between(3.00, 10.00)

    def _get_mayan_api_token(self):
        return self.environment.parsed_options.__dict__[
            ARGUMENTS_MAYAN_EDMS_API_TOKEN_DEST
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.taskset_factory.populate(user=self)

    def on_start(self):
        headers = {
            'Authorization': 'Token {}'.format(self._get_mayan_api_token())
        }

        self.client.headers.update(headers)
